# AHKollection

Collection of AutoHotkey scripts I've written for various little tasks.

## Quick Clip

It's easier done than said, it's Quick Clip!

Sometimes I need to copy and paste a bunch of data (like item barcodes) from one place into a text file so I can import them into another place (like Evergreen's Item Status window). That's why I created Quick Clip. It takes data on the clipboard and writes it directly to a text file in one keystroke.

### The Shortcuts

`Ctrl` + `Q`: Quick Clip. This creates a single text file (by default called `quickclip.txt` in your Desktop folder) and will continually add (append) more data to that file.

`Ctrl` + `Alt` +  `Q`: Serial Quick Clip. This will create separate files every time you use it in case you need to upload portions of your data in chunks or simply want to perform several clips in rapid succession..

`Ctrl` + `Alt` + `Shift` + `Q`: Recycle your clips. Calling this will send *all* clips to the Recycle Bin in the Quick Clips directory (by default the Desktop). Use with caution!