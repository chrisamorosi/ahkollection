#NoEnv
#SingleInstance Force
#MaxThreadsPerHotkey 2

; Settings

QC_DIR := A_Desktop
; QC_DIR := %A_MyDocuments%
QC_FILENAME := "quickclip"

; Ctrl + Q: Quick Clip Concatenate
; Appends whatever is in the clipboard to a single quickclip text file, 
; creating it if necessary.

^q::
    FileAppend, % Clipboard "`n", %QC_DIR%\%QC_FILENAME%.txt
return

; Ctrl + Alt + Q: Quick Clip Serially
; Creates separate files for each clip, using a timestamp to reasonably
; ensure a new file is created each time.

^!q::
	FileAppend, % Clipboard, %A_Desktop%\%QC_FILENAME%_%A_Hour%%A_Min%%A_Sec%.txt
return

; Ctrl + Alt + Shift + Q: Quick Clip recycle
; Sends all Quick Clip files in QC_DIR to the Recycle Bin.

^!+q::
    FileRecycle, %QC_DIR%\%QC_FILENAME%*.txt
return